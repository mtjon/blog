---
title: "About"
date: 2018-08-06T07:06:05Z
draft: true
---

A personal blog to structure my&mdash;mostly tech-related&mdash;ideas and hopefully show an interesting development of topics in hindsight.  
In case I bother to maintain this page long enough to create in-depth posts, they might also be useful others. Which would be great.

# About me

Martin Tjon. Tech Consultant. Economist. The Netherlands.
