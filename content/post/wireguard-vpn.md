---
title: "Rolling your own GuardWire VPN-server"
date: 2018-08-06T05:40:08Z
draft: true
---

![It's dangerous to go alone.][zelda_dangerous_alone]

Everybody needs a VPN in their life. Because reasons:

- security;
- privacy;
- convenience;
- bragging rights.

Given above reasons, I argue that it makes sense to roll your own. I chose for a [_WireGuard_][wireguard-home]-based implementation through [_algo_][github-algo]. Let's get into that.

## Security
Security is a nuisance, but I try to take is seriously.

## Privacy
Privacy is considered a constitunial right in my country. That does not mean it is free. A VPN helps in maintaining some of it, you should pay for it.


# So&hellip; how to proceed?

[zelda_dangerous_alone]: /images/zelda_dangerous_alone.png
[wireguard-home]: https://www.wireguard.com/
[github-algo]: https://github.com/trailofbits/algo
